/*
 * Created by Martin Endrst as part of MI-MDW course at CTU FIT
 */
package model;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlSeeAlso;

/**
 * @author Martin Endrst
 */
@XmlSeeAlso({FlightInfo.class, TripInfo.class})
public class BookingInfo implements Serializable {
    
    public int id;
    
    public FlightInfo flightInfo;
    public TripInfo tripInfo;
    
    public BookingInfo() { }
    public BookingInfo(int id){
        this.id = id;
    }
    
}
