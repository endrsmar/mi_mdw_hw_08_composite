/*
 * Created by Martin Endrst as part of MI-MDW course at CTU FIT
 */
package model;

/**
 * @author Martin Endrst
 */
public class TripInfo {
    
    public String destination;
    public double price;
    
    public TripInfo() { }
    public TripInfo(String destination, double price){
        this.destination = destination;
        this.price = price;
    }
    
}
