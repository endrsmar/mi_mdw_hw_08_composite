/*
 * Created by Martin Endrst as part of MI-MDW course at CTU FIT
 */
package model;

import java.util.Date;

/**
 * @author Martin Endrst
 */
public class FlightInfo {
    
    public String departureAirport;
    public Date departureDate;
    public String arrivalAirport;
    public Date arrivalDate;
    
    public FlightInfo() {}
    public FlightInfo(String departureAirport, Date departureDate, String arrivalAirport, Date arrivalDate){
        this.departureAirport = departureAirport;
        this.departureDate = departureDate;
        this.arrivalAirport = arrivalAirport;
        this.arrivalDate = arrivalDate;
    }
    
}
