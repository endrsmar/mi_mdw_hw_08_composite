/*
 * Created by Martin Endrst as part of MI-MDW course at CTU FIT
 */
package webservice;

import fault.CapacityInsufficient;
import fault.FlightNotFound;
import fault.InvalidType;
import fault.TripNotFound;
import flight_service.Flight;
import flight_service.FlightBooking;
import flight_service.FlightNotFound_Exception;
import flight_service.FlightService_Service;
import java.util.List;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.xml.ws.Holder;
import javax.xml.ws.WebServiceRef;
import model.BookingInfo;
import model.FlightInfo;
import model.TripInfo;
import trip_service.Trip;
import trip_service.TripBooking;
import trip_service.TripNotFound_Exception;
import trip_service.TripService_Service;

/**
 *
 * @author Martin Endrst
 */
@WebService(serviceName = "CompositeService")
public class CompositeService {
//
    @WebServiceRef(wsdlLocation = "http://localhost:7001/MDW_HW_8_Trip/TripService?WSDL")
    private TripService_Service service_1;

    @WebServiceRef(wsdlLocation = "http://localhost:7001/MDW_HW_8_Flight/FlightService?WSDL")
    private FlightService_Service service;

    @WebMethod(operationName = "makeBooking")
    @WebResult(name = "booking")
    public BookingInfo makeBooking(@WebParam(name = "type") String type,
                                  @WebParam(name = "name") String name,
                                  @WebParam(name = "destination") String destination) throws InvalidType, CapacityInsufficient, FlightNotFound, TripNotFound {
        if (type.equals("flight")){
            Flight f = null;
            for (Flight flight : listFlights()){
                if (flight.getDestination().equals(destination)){
                    f = flight;
                    break;
                }
            }
            if (f == null) { throw new FlightNotFound(); }
            FlightBooking booking = null;
            try {
                booking = createFlightBooking(name, f.getId());
            } catch(flight_service.CapacityInsufficient_Exception e){
                throw new CapacityInsufficient();
            } catch(flight_service.FlightNotFound_Exception e){
                throw new FlightNotFound();
            }
            FlightInfo info = new FlightInfo(f.getDepartureAirport(), f.getDepartureDate().toGregorianCalendar().getTime(), f.getArrivalAirport(), f.getArrivalDate().toGregorianCalendar().getTime());
            BookingInfo bookingInfo = new BookingInfo(booking.getId());
            bookingInfo.flightInfo = info;
            return bookingInfo;
        } else if (type.equals("trip")) {
            Trip t = null;
            for (Trip trip : listTrips()){
                if (trip.getDestination().equals(destination)){
                    t = trip;
                    break;
                }
            }
            if (t == null) { throw new TripNotFound(); }
            TripBooking booking = null;
            try {
                booking = createTripBooking(t.getId(), name, 1);
            } catch(trip_service.CapacityInsufficient_Exception e){
                throw new CapacityInsufficient();
            } catch(trip_service.TripNotFound_Exception e){
                throw new TripNotFound();
            }
            TripInfo info = new TripInfo(t.getDestination(), t.getPrice());
            BookingInfo bookingInfo = new BookingInfo(booking.getId());
            bookingInfo.tripInfo = info;
            return bookingInfo;
        } else {
            throw new InvalidType();
        }
    }

    private List<Flight> listFlights() {
        flight_service.FlightService port = service.getFlightServicePort();
        return port.listFlights();
    }

    private FlightBooking createFlightBooking(String name, int flightId) throws FlightNotFound_Exception, flight_service.CapacityInsufficient_Exception {
        flight_service.FlightService port = service.getFlightServicePort();
        return port.createFlightBooking(name, flightId);
    }

    private TripBooking createTripBooking(int tripId, java.lang.String name, int persons) throws TripNotFound_Exception, trip_service.CapacityInsufficient_Exception {
        trip_service.TripService port = service_1.getTripServicePort();
        return port.createTripBooking(tripId, name, persons);
    }

    private List<Trip> listTrips() {
        trip_service.TripService port = service_1.getTripServicePort();
        return port.listTrips();
    }
    
}
