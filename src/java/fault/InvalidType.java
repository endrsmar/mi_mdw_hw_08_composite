/*
 * Created by Martin Endrst as part of MI-MDW course at CTU FIT
 */
package fault;

/**
 * @author Martin Endrst
 */
public class InvalidType extends Exception {
    public InvalidType(){
        super("Invalid type");
    }
}
