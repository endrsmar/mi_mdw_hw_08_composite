/*
 * Created by Martin Endrst as part of MI-MDW course at CTU FIT
 */
package fault;

/**
 * @author Martin Endrst
 */
public class TripNotFound extends Exception {
    
    public TripNotFound(){
        super("Trip not found");
    }
    
}
