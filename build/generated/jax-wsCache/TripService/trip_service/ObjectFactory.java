
package trip_service;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the trip_service package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CreateTrip_QNAME = new QName("http://trip_service/", "createTrip");
    private final static QName _ListTrips_QNAME = new QName("http://trip_service/", "listTrips");
    private final static QName _CapacityInsufficient_QNAME = new QName("http://trip_service/", "CapacityInsufficient");
    private final static QName _CreateTripBookingResponse_QNAME = new QName("http://trip_service/", "createTripBookingResponse");
    private final static QName _TripNotFound_QNAME = new QName("http://trip_service/", "TripNotFound");
    private final static QName _CreateTripBooking_QNAME = new QName("http://trip_service/", "createTripBooking");
    private final static QName _ListTripsResponse_QNAME = new QName("http://trip_service/", "listTripsResponse");
    private final static QName _ListTripBookings_QNAME = new QName("http://trip_service/", "listTripBookings");
    private final static QName _CreateTripResponse_QNAME = new QName("http://trip_service/", "createTripResponse");
    private final static QName _ListTripBookingsResponse_QNAME = new QName("http://trip_service/", "listTripBookingsResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: trip_service
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CreateTripResponse }
     * 
     */
    public CreateTripResponse createCreateTripResponse() {
        return new CreateTripResponse();
    }

    /**
     * Create an instance of {@link ListTripBookingsResponse }
     * 
     */
    public ListTripBookingsResponse createListTripBookingsResponse() {
        return new ListTripBookingsResponse();
    }

    /**
     * Create an instance of {@link CreateTripBooking }
     * 
     */
    public CreateTripBooking createCreateTripBooking() {
        return new CreateTripBooking();
    }

    /**
     * Create an instance of {@link ListTripsResponse }
     * 
     */
    public ListTripsResponse createListTripsResponse() {
        return new ListTripsResponse();
    }

    /**
     * Create an instance of {@link TripNotFound }
     * 
     */
    public TripNotFound createTripNotFound() {
        return new TripNotFound();
    }

    /**
     * Create an instance of {@link ListTripBookings }
     * 
     */
    public ListTripBookings createListTripBookings() {
        return new ListTripBookings();
    }

    /**
     * Create an instance of {@link CapacityInsufficient }
     * 
     */
    public CapacityInsufficient createCapacityInsufficient() {
        return new CapacityInsufficient();
    }

    /**
     * Create an instance of {@link CreateTripBookingResponse }
     * 
     */
    public CreateTripBookingResponse createCreateTripBookingResponse() {
        return new CreateTripBookingResponse();
    }

    /**
     * Create an instance of {@link CreateTrip }
     * 
     */
    public CreateTrip createCreateTrip() {
        return new CreateTrip();
    }

    /**
     * Create an instance of {@link ListTrips }
     * 
     */
    public ListTrips createListTrips() {
        return new ListTrips();
    }

    /**
     * Create an instance of {@link TripBooking }
     * 
     */
    public TripBooking createTripBooking() {
        return new TripBooking();
    }

    /**
     * Create an instance of {@link Trip }
     * 
     */
    public Trip createTrip() {
        return new Trip();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateTrip }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://trip_service/", name = "createTrip")
    public JAXBElement<CreateTrip> createCreateTrip(CreateTrip value) {
        return new JAXBElement<CreateTrip>(_CreateTrip_QNAME, CreateTrip.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListTrips }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://trip_service/", name = "listTrips")
    public JAXBElement<ListTrips> createListTrips(ListTrips value) {
        return new JAXBElement<ListTrips>(_ListTrips_QNAME, ListTrips.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CapacityInsufficient }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://trip_service/", name = "CapacityInsufficient")
    public JAXBElement<CapacityInsufficient> createCapacityInsufficient(CapacityInsufficient value) {
        return new JAXBElement<CapacityInsufficient>(_CapacityInsufficient_QNAME, CapacityInsufficient.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateTripBookingResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://trip_service/", name = "createTripBookingResponse")
    public JAXBElement<CreateTripBookingResponse> createCreateTripBookingResponse(CreateTripBookingResponse value) {
        return new JAXBElement<CreateTripBookingResponse>(_CreateTripBookingResponse_QNAME, CreateTripBookingResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TripNotFound }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://trip_service/", name = "TripNotFound")
    public JAXBElement<TripNotFound> createTripNotFound(TripNotFound value) {
        return new JAXBElement<TripNotFound>(_TripNotFound_QNAME, TripNotFound.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateTripBooking }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://trip_service/", name = "createTripBooking")
    public JAXBElement<CreateTripBooking> createCreateTripBooking(CreateTripBooking value) {
        return new JAXBElement<CreateTripBooking>(_CreateTripBooking_QNAME, CreateTripBooking.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListTripsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://trip_service/", name = "listTripsResponse")
    public JAXBElement<ListTripsResponse> createListTripsResponse(ListTripsResponse value) {
        return new JAXBElement<ListTripsResponse>(_ListTripsResponse_QNAME, ListTripsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListTripBookings }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://trip_service/", name = "listTripBookings")
    public JAXBElement<ListTripBookings> createListTripBookings(ListTripBookings value) {
        return new JAXBElement<ListTripBookings>(_ListTripBookings_QNAME, ListTripBookings.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateTripResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://trip_service/", name = "createTripResponse")
    public JAXBElement<CreateTripResponse> createCreateTripResponse(CreateTripResponse value) {
        return new JAXBElement<CreateTripResponse>(_CreateTripResponse_QNAME, CreateTripResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListTripBookingsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://trip_service/", name = "listTripBookingsResponse")
    public JAXBElement<ListTripBookingsResponse> createListTripBookingsResponse(ListTripBookingsResponse value) {
        return new JAXBElement<ListTripBookingsResponse>(_ListTripBookingsResponse_QNAME, ListTripBookingsResponse.class, null, value);
    }

}
