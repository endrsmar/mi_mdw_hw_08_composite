
package flight_service;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the flight_service package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ListFlightBookingsResponse_QNAME = new QName("http://flight_service/", "listFlightBookingsResponse");
    private final static QName _FlightBookingNotFound_QNAME = new QName("http://flight_service/", "FlightBookingNotFound");
    private final static QName _CreateFlightBooking_QNAME = new QName("http://flight_service/", "createFlightBooking");
    private final static QName _CapacityInsufficient_QNAME = new QName("http://flight_service/", "CapacityInsufficient");
    private final static QName _RemoveFlightBookingResponse_QNAME = new QName("http://flight_service/", "removeFlightBookingResponse");
    private final static QName _ListFlights_QNAME = new QName("http://flight_service/", "listFlights");
    private final static QName _CreateFlightBookingResponse_QNAME = new QName("http://flight_service/", "createFlightBookingResponse");
    private final static QName _ListFlightsResponse_QNAME = new QName("http://flight_service/", "listFlightsResponse");
    private final static QName _ListFlightBookings_QNAME = new QName("http://flight_service/", "listFlightBookings");
    private final static QName _RemoveFlightBooking_QNAME = new QName("http://flight_service/", "removeFlightBooking");
    private final static QName _FlightNotFound_QNAME = new QName("http://flight_service/", "FlightNotFound");
    private final static QName _CreateFlight_QNAME = new QName("http://flight_service/", "createFlight");
    private final static QName _CreateFlightResponse_QNAME = new QName("http://flight_service/", "createFlightResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: flight_service
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CreateFlightBookingResponse }
     * 
     */
    public CreateFlightBookingResponse createCreateFlightBookingResponse() {
        return new CreateFlightBookingResponse();
    }

    /**
     * Create an instance of {@link ListFlightsResponse }
     * 
     */
    public ListFlightsResponse createListFlightsResponse() {
        return new ListFlightsResponse();
    }

    /**
     * Create an instance of {@link ListFlightBookings }
     * 
     */
    public ListFlightBookings createListFlightBookings() {
        return new ListFlightBookings();
    }

    /**
     * Create an instance of {@link RemoveFlightBooking }
     * 
     */
    public RemoveFlightBooking createRemoveFlightBooking() {
        return new RemoveFlightBooking();
    }

    /**
     * Create an instance of {@link FlightNotFound }
     * 
     */
    public FlightNotFound createFlightNotFound() {
        return new FlightNotFound();
    }

    /**
     * Create an instance of {@link CreateFlight }
     * 
     */
    public CreateFlight createCreateFlight() {
        return new CreateFlight();
    }

    /**
     * Create an instance of {@link CreateFlightResponse }
     * 
     */
    public CreateFlightResponse createCreateFlightResponse() {
        return new CreateFlightResponse();
    }

    /**
     * Create an instance of {@link FlightBookingNotFound }
     * 
     */
    public FlightBookingNotFound createFlightBookingNotFound() {
        return new FlightBookingNotFound();
    }

    /**
     * Create an instance of {@link ListFlightBookingsResponse }
     * 
     */
    public ListFlightBookingsResponse createListFlightBookingsResponse() {
        return new ListFlightBookingsResponse();
    }

    /**
     * Create an instance of {@link CreateFlightBooking }
     * 
     */
    public CreateFlightBooking createCreateFlightBooking() {
        return new CreateFlightBooking();
    }

    /**
     * Create an instance of {@link CapacityInsufficient }
     * 
     */
    public CapacityInsufficient createCapacityInsufficient() {
        return new CapacityInsufficient();
    }

    /**
     * Create an instance of {@link RemoveFlightBookingResponse }
     * 
     */
    public RemoveFlightBookingResponse createRemoveFlightBookingResponse() {
        return new RemoveFlightBookingResponse();
    }

    /**
     * Create an instance of {@link ListFlights }
     * 
     */
    public ListFlights createListFlights() {
        return new ListFlights();
    }

    /**
     * Create an instance of {@link FlightBooking }
     * 
     */
    public FlightBooking createFlightBooking() {
        return new FlightBooking();
    }

    /**
     * Create an instance of {@link Flight }
     * 
     */
    public Flight createFlight() {
        return new Flight();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListFlightBookingsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://flight_service/", name = "listFlightBookingsResponse")
    public JAXBElement<ListFlightBookingsResponse> createListFlightBookingsResponse(ListFlightBookingsResponse value) {
        return new JAXBElement<ListFlightBookingsResponse>(_ListFlightBookingsResponse_QNAME, ListFlightBookingsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FlightBookingNotFound }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://flight_service/", name = "FlightBookingNotFound")
    public JAXBElement<FlightBookingNotFound> createFlightBookingNotFound(FlightBookingNotFound value) {
        return new JAXBElement<FlightBookingNotFound>(_FlightBookingNotFound_QNAME, FlightBookingNotFound.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateFlightBooking }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://flight_service/", name = "createFlightBooking")
    public JAXBElement<CreateFlightBooking> createCreateFlightBooking(CreateFlightBooking value) {
        return new JAXBElement<CreateFlightBooking>(_CreateFlightBooking_QNAME, CreateFlightBooking.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CapacityInsufficient }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://flight_service/", name = "CapacityInsufficient")
    public JAXBElement<CapacityInsufficient> createCapacityInsufficient(CapacityInsufficient value) {
        return new JAXBElement<CapacityInsufficient>(_CapacityInsufficient_QNAME, CapacityInsufficient.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveFlightBookingResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://flight_service/", name = "removeFlightBookingResponse")
    public JAXBElement<RemoveFlightBookingResponse> createRemoveFlightBookingResponse(RemoveFlightBookingResponse value) {
        return new JAXBElement<RemoveFlightBookingResponse>(_RemoveFlightBookingResponse_QNAME, RemoveFlightBookingResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListFlights }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://flight_service/", name = "listFlights")
    public JAXBElement<ListFlights> createListFlights(ListFlights value) {
        return new JAXBElement<ListFlights>(_ListFlights_QNAME, ListFlights.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateFlightBookingResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://flight_service/", name = "createFlightBookingResponse")
    public JAXBElement<CreateFlightBookingResponse> createCreateFlightBookingResponse(CreateFlightBookingResponse value) {
        return new JAXBElement<CreateFlightBookingResponse>(_CreateFlightBookingResponse_QNAME, CreateFlightBookingResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListFlightsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://flight_service/", name = "listFlightsResponse")
    public JAXBElement<ListFlightsResponse> createListFlightsResponse(ListFlightsResponse value) {
        return new JAXBElement<ListFlightsResponse>(_ListFlightsResponse_QNAME, ListFlightsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListFlightBookings }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://flight_service/", name = "listFlightBookings")
    public JAXBElement<ListFlightBookings> createListFlightBookings(ListFlightBookings value) {
        return new JAXBElement<ListFlightBookings>(_ListFlightBookings_QNAME, ListFlightBookings.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveFlightBooking }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://flight_service/", name = "removeFlightBooking")
    public JAXBElement<RemoveFlightBooking> createRemoveFlightBooking(RemoveFlightBooking value) {
        return new JAXBElement<RemoveFlightBooking>(_RemoveFlightBooking_QNAME, RemoveFlightBooking.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FlightNotFound }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://flight_service/", name = "FlightNotFound")
    public JAXBElement<FlightNotFound> createFlightNotFound(FlightNotFound value) {
        return new JAXBElement<FlightNotFound>(_FlightNotFound_QNAME, FlightNotFound.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateFlight }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://flight_service/", name = "createFlight")
    public JAXBElement<CreateFlight> createCreateFlight(CreateFlight value) {
        return new JAXBElement<CreateFlight>(_CreateFlight_QNAME, CreateFlight.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateFlightResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://flight_service/", name = "createFlightResponse")
    public JAXBElement<CreateFlightResponse> createCreateFlightResponse(CreateFlightResponse value) {
        return new JAXBElement<CreateFlightResponse>(_CreateFlightResponse_QNAME, CreateFlightResponse.class, null, value);
    }

}
